using System;

namespace PracticeTasks.Task2
{
    public class Program
    {
        public static void Main()
        {
            // Instantiation
            var polynomial1 = new Polynomial<int>( new int[]{ 2, 5, 1, 0, 4 });
            var polynomial2 = new Polynomial<int>( new int[]{ 1, 3, 1, 2, 2 });
            var sparsePolynomial = new Polynomial<double>(new Polynomial<double>.PolynomialItem(5, 2),
                                                          new Polynomial<double>.PolynomialItem(2, 10),
                                                          new Polynomial<double>.PolynomialItem(2, 22) );
                                                          
            Console.WriteLine("==== Simple polynomials");
            Console.WriteLine(polynomial1);
            Console.WriteLine(polynomial2);
            Console.WriteLine("==== Sparse polynomial");
            Console.WriteLine(sparsePolynomial);
            Console.WriteLine();       

            // Arithmetic operations demo   
            Console.WriteLine("==== Two simple polynomial added");
            Console.WriteLine(polynomial1 + polynomial2);
            Console.WriteLine();

            Console.WriteLine("==== Two simple polynomial subtracted");
            Console.WriteLine(polynomial1 - polynomial2);
            Console.WriteLine();

            Console.WriteLine("==== Two simple polynomial multiplied");
            Console.WriteLine(polynomial1 * polynomial2);             
            Console.WriteLine();

            // Substitution
            Console.WriteLine("==== Sparse polynomial if x == 2");
            Console.WriteLine(sparsePolynomial.DoSubstitution(2));
        }
    }
}